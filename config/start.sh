#!/bin/bash

yamllint  -f colored -d "{extends: default, rules: {$exclude}, ignore: .gitlab-ci.yml}" . 1 > err.txt

err=$(cat err.txt)
echo "$err"
failure=$(echo "$err" | grep "error" | wc -l)
warning=$(echo "$err" | grep "warning" | wc -l)

if (( $failure+$warning==0 )); then
state="0"

elif (( $failure>0 )); then
state="1"

else
state="3"

fi


NC='\033[0m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
LBLUE='\033[1;34m'
echo -e "\n${LBLUE}$failure ${RED}failures${NC}, ${LBLUE}$warning ${YELLOW}warnings${NC}\n"



if ! ([ -z "$TOKEN" ] || [ -z "$CI_JOB_URL" ] || [ -z "$CI_SERVER_HOST" ] || [ -z "$CI_API_V4_URL" ] || [ -z "$CI_PROJECT_ID" ])
then

# rm -rf /project/playbook/.cache;
# # initialize if he find file .yml
# YML=$(find "$PWD" -name '*.yml'|head -n1)
# if ([ ! -z "$YML" ])
# then
# /project/config/function.py init_badge
# fi

img_url="${BASE_PROJECT_BADGE:-"https://gitlab.com/davtali"}/yamllint/raw/main/badge/yamllint_"
/project/config/function.py init_badge $img_url

id=$(/project/config/function.py get_id 2>&1 > /dev/null);
# redirect print to null for send id by stderr, for don't show this output on pipeline ci

# -v
curl --silent --output /dev/null --request PUT --header "PRIVATE-TOKEN: $TOKEN" \
-d "link_url=${CI_JOB_URL}" \
-d "image_url=${img_url}${state}.svg" \
${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/badges/${id};
fi
