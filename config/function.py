#!/usr/local/bin/python                                          
                                                                                                                                                                                    
import os                                                                                                                                                                           
import requests                                                                                                                                                                     
import sys                                                                                                                                                                          
                                                                                                                                                                                    
def get_id():
    '''
    get id by parsing string
    because we can't chose the id, and was only increment
    '''
    badges=os.environ['CI_API_V4_URL']+"/projects/"+os.environ['CI_PROJECT_ID']+"/badges"
    rep=requests.get(badges,headers={"PRIVATE-TOKEN": os.environ['TOKEN']}).json()

    for badge in rep:                                                                                                                                                                                                                                                                                                                        
        tmp=badge['image_url']
        select=tmp[tmp.rindex('/')+1:tmp.rindex('_')]                                                                                                                               
        if select==os.environ['CI_JOB_NAME']:                                                                                                                                                                                                                                                                                            
             sys.exit(str(badge['id']))                                                                                                                                               


def init_badge(img_url):
    '''
    initialize badge ansible-lint if no exist already
    the link initilialize is necessary, but the value was not important because he was updated
    after commit for redirect to the last job
    ---------------
    input: str image image_url
    '''
	
    header={"PRIVATE-TOKEN": os.environ['TOKEN']}

    url=os.environ['CI_API_V4_URL']+"/projects/"+os.environ['CI_PROJECT_ID']+"/badges"
    rep=requests.get(url=url, headers=header).json()

    # check if project have already badge "yamllint"
    for badge in rep:
        tmp=badge['image_url']
        select=tmp[tmp.rindex('/')+1:tmp.rindex('_')]
        if select=="yamllint": return 0

    # "http://"+os.environ['CI_SERVER_HOST']+"/Docker/lint/yamllint/raw/master/badge/yamllint_2.svg"
    dico={"image_url":img_url+"2.svg",
        "link_url":"http://"+os.environ['CI_SERVER_HOST']+"/Docker/lint/yamllint"
    }
    requests.post(url=url, headers=header, data=dico)

if __name__ == "__main__":
    args=sys.argv
    globals()[args[1]](*args[2:])


