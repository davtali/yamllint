FROM python:3

ENV BASE_PROJECT_BADGE ""
ARG YAMLLINT_VERSION=1.26.1

COPY config/ /project/config/
COPY badge/ /project/badge/
RUN pip install --upgrade pip \
	&& pip install --no-cache --upgrade pip \
	&& pip install --no-cache requests yamllint==${YAMLLINT_VERSION}; \
	chmod +x /project/config/start.sh; \
	chmod +x /project/config/function.py;

WORKDIR /project/playbook
ENTRYPOINT ["/project/config/start.sh"]

	


