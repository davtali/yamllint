![](https://gitlab.com/davtali/yamllint/raw/main/badge/yamllint_0.svg)  ![](https://gitlab.com/davtali/yamllint/raw/main/badge/yamllint_3.svg)  ![](https://gitlab.com/davtali/yamllint/raw/main/badge/yamllint_1.svg) ![](https://gitlab.com/davtali/yamllint/raw/main/badge/yamllint_2.svg) \
Was tested with yamllint 1.26.1 \
The project add badge for you gitlab project. \
The color of badge depends on the result lint. \
The lint give your the best bratice for project who use YAML syntax. \
The lint was applied on full project \
The badge redirect to the last job where we can see the result lint on terminal \
\
The image badge was host on project, they can work without artefact, library and on offline in private gitlab.



you can show example on the project : [here](https://gitlab.com/davtali/ansible-mongodb)

For show an alternative version to add dynamically badge yamllint: [here](https://gitlab.com/davtali/yamllint_alt)

For add badge ansible-lint here [here](https://gitlab.com/davtali/ansible-lint)



# Quick start

You must create token with scope api on gitlab on your user Setting Account (`user_settings>acces_tokens`). \
Copy your secret token, create variable (`your_project>settings>CICD>variables`) with key=TOKEN and past your token on VALUE 

Add this instruction in your .gitlab-ci.yml

```
yamllint:
  stage: lint
  image:
    name: dal3ap1/yamllint:latest
    entrypoint: [""]
  variables:
  script:
    - /project/config/start.sh
```
You can specify the rules who you want ignore it on lint \

```
yamllint:
  stage: lint
  image:
    name: dal3ap1/yamllint:latest
    entrypoint: [""]
  variables:
    exclude: "line-length: disable,
              comments: disable, 
              empty-lines: disable"
  script:
    - /project/config/start.sh
```

for check all the rules show documentation yamllint.


# Self host

You must overload the BASE_PROJECT_BADGE.

If you rebuild image, you can overload directly on the Dockefile. \
It is the path before your location ansible-lint. (https://gitlab.com/davtali if project was https://gitlab.com/davtali/yamllint)
```
ENV BASE_PROJECT_BADGE "https://gitlab.com/username/my_groups/my_subgroup"
# ENV BASE_PROJECT_BADGE "https://gitlab.com/davtali"
```
`docker build -t ansible-lint .`

Or you can overload the BASE_PROJECT_BADGE on the CI.

```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint:latest
    entrypoint: [""]
  variable:
    exclude: "meta-video-links,deprecated-module"
    BASE_PROJECT_BADGE: ""https://gitlab.com/username/my_groups/my_subgroup"
    #BASE_PROJECT_BADGE: "https://gitlab.com/davtali"
  script:
    - /project/config/start.sh
```

# Use without ci

For use only the lint part:
```
docker run --rm -v $PWD:/project/playbook dal3ap1/yamllint:latest
```
You can specify the rules who you want ignore it on lint \
```
docker run --rm -v $PWD:/project/playbook -e exclude="line-length: disable, comments: disable, empty-lines: disable" dal3ap1/yamllint:latest
```




